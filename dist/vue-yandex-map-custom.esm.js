//
//
//
//

var script = {
    props: {
        apiKey: {
            type: String,
        },
        coordinatesDefault: {
            type: Array,
            default: function () { return [55.751244, 37.618423]; },
        },
        getAdress: {
            type: Boolean,
            default: true
        },
        placemarkDraggable: {
            type: Boolean,
            default: true
        },
        placemarkClick: {
            type: Boolean,
            default: true
        },
        getAdress: {
            type: Boolean,
            default: true
        },

    },
    data: function data() {
        return {
            map: null,
            address: null,
            placemark: null,
            searchControl: null,
            coordinates: this.coordinatesDefault,
        }
    },
    mounted: function mounted() {
        // Установливаем скрипты для использования яндекс карты
        var scriptYandexMap = document.createElement('script');
        scriptYandexMap.setAttribute('src', ("https://api-maps.yandex.ru/2.1/?apikey=" + (this.apiKey) + "&lang=ru_RU"));
        document.head.appendChild(scriptYandexMap);

        // Инициализируем яндекс карту
        scriptYandexMap.addEventListener("load", this.initializeYandexMap);
    },
    watch: {
        coordinates: function coordinates(v) {
            this.geocodeAdress();
            this.placemark.geometry.setCoordinates(v);
        },
        address: function address(v) {
            this.$emit('address', v);
        },
    },
    methods: {
        initializeYandexMap: function initializeYandexMap() {
            var this$1 = this;

            ymaps.ready(function () {
                this$1.map = new ymaps.Map("yandex-map", {
                    center: this$1.coordinates,
                    zoom: 10,
                    controls: ['zoomControl', 'fullscreenControl'],
                    searchControlProvider: 'yandex#search'
                });

                // Отключаем скрол карты
                this$1.map.behaviors.disable('scrollZoom');

                // Добавляем свой маркер
                this$1.setPlacemark();

                // Добавляем поиск
                this$1.addSearchControl();

                // Добавляем слушатель на результаты поиска
                this$1.listenerResultSearch();

                // Добавляем слушатель на перетаскивание маркера
                this$1.listenerMovePlacemark();

                // Добавляем слушатель на клик по карте
                this$1.listenerClickByMap();
            });
        },
        setPlacemark: function setPlacemark() {
            this.placemark = new ymaps.Placemark(
                this.coordinates,
                {},
                {draggable: this.placemarkDraggable}
            );

            this.map.geoObjects.add(this.placemark);
            this.geocodeAdress();
        },
        addSearchControl: function addSearchControl() {
            this.searchControl = new ymaps.control.SearchControl({
                options: {
                    // Скрываем маркер поиска
                    noPlacemark: true,
                    // Количество объектов на странице
                    resultsPerPage: 1
                },
            });

            this.map.controls.add(this.searchControl);
        },
        listenerResultSearch: function listenerResultSearch() {
            var this$1 = this;

            this.searchControl.events.add("resultselect", function (e) {
                this$1.coordinates = this$1.searchControl.getResultsArray()[0].geometry.getCoordinates();
            });
        },
        listenerMovePlacemark: function listenerMovePlacemark() {
            var this$1 = this;

            if (this.placemarkDraggable)
                { this.placemark.events.add("dragend", function (e) {
                    this$1.coordinates = this$1.placemark.geometry.getCoordinates();
                }); }
        },
        listenerClickByMap: function listenerClickByMap() {
            var this$1 = this;

            if (this.placemarkClick)
                { this.map.events.add("click", function (e) {
                    this$1.coordinates = e.get('coords');
                }); }
        },
        geocodeAdress: function geocodeAdress() {
            var this$1 = this;

            if (this.getAdress) {
                this.placemark.properties.set('iconCaption', 'поиск...');

                ymaps.geocode(this.coordinates).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);

                    this$1.placemark.properties.set({
                        // Формируем строку с данными об объекте.
                        iconCaption: [

                            // Название населенного пункта или вышестоящее административно-территориальное образование.
                            firstGeoObject.getLocalities().length
                                ? firstGeoObject.getLocalities()
                                : firstGeoObject.getAdministrativeAreas(),

                            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                        ].filter(Boolean).join(', '),
                        // В качестве контента балуна задаем строку с адресом объекта.
                        balloonContent: firstGeoObject.getAddressLine()
                    });

                    this$1.address = firstGeoObject.getAddressLine();
                });
            }
        }
    }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    var options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    var hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            var originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            var existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

var isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return function (id, style) { return addStyle(id, style); };
}
var HEAD;
var styles = {};
function addStyle(id, css) {
    var group = isOldIE ? css.media || 'default' : id;
    var style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        var code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                { style.element.setAttribute('media', css.media); }
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            var index = style.ids.size - 1;
            var textNode = document.createTextNode(code);
            var nodes = style.element.childNodes;
            if (nodes[index])
                { style.element.removeChild(nodes[index]); }
            if (nodes.length)
                { style.element.insertBefore(textNode, nodes[index]); }
            else
                { style.element.appendChild(textNode); }
        }
    }
}

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", { attrs: { id: "yandex-map" } })
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-2f94f7cc_0", { source: "\n#yandex-map {\n    width: 100vw;\n    height: 100vh;\n}\n", map: {"version":3,"sources":["/Users/tkach/projects/packages/vue-yandex-map-v2/src/vue-yandex-map-custom.vue"],"names":[],"mappings":";AA+JA;IACA,YAAA;IACA,aAAA;AACA","file":"vue-yandex-map-custom.vue","sourcesContent":["<template>\n    <div id=\"yandex-map\"></div>\n</template>\n\n<script>\n    export default {\n        props: {\n            apiKey: {\n                type: String,\n            },\n            coordinatesDefault: {\n                type: Array,\n                default: () => [55.751244, 37.618423],\n            },\n            getAdress: {\n                type: Boolean,\n                default: true\n            },\n            placemarkDraggable: {\n                type: Boolean,\n                default: true\n            },\n            placemarkClick: {\n                type: Boolean,\n                default: true\n            },\n            getAdress: {\n                type: Boolean,\n                default: true\n            },\n\n        },\n        data() {\n            return {\n                map: null,\n                address: null,\n                placemark: null,\n                searchControl: null,\n                coordinates: this.coordinatesDefault,\n            }\n        },\n        mounted() {\n            // Установливаем скрипты для использования яндекс карты\n            let scriptYandexMap = document.createElement('script');\n            scriptYandexMap.setAttribute('src', `https://api-maps.yandex.ru/2.1/?apikey=${this.apiKey}&lang=ru_RU`);\n            document.head.appendChild(scriptYandexMap);\n\n            // Инициализируем яндекс карту\n            scriptYandexMap.addEventListener(\"load\", this.initializeYandexMap);\n        },\n        watch: {\n            coordinates(v) {\n                this.geocodeAdress();\n                this.placemark.geometry.setCoordinates(v);\n            },\n            address(v) {\n                this.$emit('address', v);\n            },\n        },\n        methods: {\n            initializeYandexMap() {\n                ymaps.ready(() => {\n                    this.map = new ymaps.Map(\"yandex-map\", {\n                        center: this.coordinates,\n                        zoom: 10,\n                        controls: ['zoomControl', 'fullscreenControl'],\n                        searchControlProvider: 'yandex#search'\n                    });\n\n                    // Отключаем скрол карты\n                    this.map.behaviors.disable('scrollZoom');\n\n                    // Добавляем свой маркер\n                    this.setPlacemark();\n\n                    // Добавляем поиск\n                    this.addSearchControl();\n\n                    // Добавляем слушатель на результаты поиска\n                    this.listenerResultSearch();\n\n                    // Добавляем слушатель на перетаскивание маркера\n                    this.listenerMovePlacemark();\n\n                    // Добавляем слушатель на клик по карте\n                    this.listenerClickByMap();\n                });\n            },\n            setPlacemark() {\n                this.placemark = new ymaps.Placemark(\n                    this.coordinates,\n                    {},\n                    {draggable: this.placemarkDraggable}\n                );\n\n                this.map.geoObjects.add(this.placemark);\n                this.geocodeAdress();\n            },\n            addSearchControl() {\n                this.searchControl = new ymaps.control.SearchControl({\n                    options: {\n                        // Скрываем маркер поиска\n                        noPlacemark: true,\n                        // Количество объектов на странице\n                        resultsPerPage: 1\n                    },\n                });\n\n                this.map.controls.add(this.searchControl);\n            },\n            listenerResultSearch() {\n                this.searchControl.events.add(\"resultselect\", (e) => {\n                    this.coordinates = this.searchControl.getResultsArray()[0].geometry.getCoordinates();\n                });\n            },\n            listenerMovePlacemark() {\n                if (this.placemarkDraggable)\n                    this.placemark.events.add(\"dragend\", (e) => {\n                        this.coordinates = this.placemark.geometry.getCoordinates();\n                    });\n            },\n            listenerClickByMap() {\n                if (this.placemarkClick)\n                    this.map.events.add(\"click\", (e) => {\n                        this.coordinates = e.get('coords');\n                    });\n            },\n            geocodeAdress() {\n                if (this.getAdress) {\n                    this.placemark.properties.set('iconCaption', 'поиск...');\n\n                    ymaps.geocode(this.coordinates).then((res) => {\n                        var firstGeoObject = res.geoObjects.get(0);\n\n                        this.placemark.properties.set({\n                            // Формируем строку с данными об объекте.\n                            iconCaption: [\n\n                                // Название населенного пункта или вышестоящее административно-территориальное образование.\n                                firstGeoObject.getLocalities().length\n                                    ? firstGeoObject.getLocalities()\n                                    : firstGeoObject.getAdministrativeAreas(),\n\n                                // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.\n                                firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()\n                            ].filter(Boolean).join(', '),\n                            // В качестве контента балуна задаем строку с адресом объекта.\n                            balloonContent: firstGeoObject.getAddressLine()\n                        });\n\n                        this.address = firstGeoObject.getAddressLine();\n                    });\n                }\n            }\n        }\n    };\n</script>\n\n<style>\n    #yandex-map {\n        width: 100vw;\n        height: 100vh;\n    }\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = undefined;
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  var __vue_component__ = normalizeComponent(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    createInjector,
    undefined,
    undefined
  );

// Импорт vue компонента

// Объявление функции установки, выполняемой Vue.use()
function install(Vue) {
    if (install.installed) { return; }
    install.installed = true;
    Vue.component('VueYandexMapV2', __vue_component__);
}

// Создание значения модуля для Vue.use()
var plugin = {
    install: install
};

// Автоматическая установка, когда vue найден (например в браузере с помощью тега <script>)
var GlobalVue = null;
if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
}
if (GlobalVue) {
    GlobalVue.use(plugin);
}

export default __vue_component__;
export { install };
